{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "sa-web-app.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "sa-web-app.image" -}}
{{- printf "ericlammers/sentiment-analysis-web-app:%s" .Chart.AppVersion | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "sa-logic.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "sa-logic.image" -}}
{{- printf "ericlammers/sentiment-analysis-logic:%s" .Chart.AppVersion | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}
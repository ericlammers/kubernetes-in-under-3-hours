{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "sa-frontend.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "sa-frontend.image" -}}
{{- printf "ericlammers/sentiment-analysis-frontend:%s" .Chart.AppVersion | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

